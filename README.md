# Simple Image Annotation Server

This project is a simple setup of [SimpleAnnotationServer](https://github.com/glenrobson/SimpleAnnotationServer)
using Docker and the Traefik proxy.

## Installation

You need [Docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/).

Create `.env` and `htusers` from `.env.template` and `htusers.template` and adjust the contents to your installation.

```
cp .env.template .env
cp htusers.template htusers
```

`VIRTUAL_HOST` in `.env` has to be your server name. 
`htusers` configures user names and passwords (see [the documentation](https://docs.traefik.io/middlewares/basicauth/)).

Then start the docker-compose setup:
```
docker-compose up -d
```

## Configuration

Annotation data is stored in the directory `annotation-data`.
