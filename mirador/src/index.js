import Mirador from 'mirador/dist/es/src/index';
import annotationPlugins from 'mirador-annotations';
import SimpleAnnotationServerV2Adapter from './SimpleAnnotationServerV2Adapter';

const config = {
  id: 'my-mirador',
  annotation: {
    adapter: (canvasId) => new SimpleAnnotationServerV2Adapter(canvasId, 'http://localhost:8080/sas/annotation'),
  },
  "selectedTheme": "dark",
  "openManifestsPage": true,
  "workspace": {
    "isWorkspaceAddVisible": true
  },
      "catalog" : [
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/sCRSn2ThGK/manifest.json", "provider": "BSB"}, /* 1565 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/Clx58Z4MAK/manifest.json", "provider": "BSB"}, /* 1568 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/vSSjKclcJf/manifest.json", "provider": "BSB"}, /* 1575 */ 
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/Xgt4kCZiWW/manifest.json", "provider": "BSB"}, /* 1586 */          
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/Yk6DrFZmub/manifest.json", "provider": "BSB"}, /* 1588 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/BA6sqBlH61/manifest.json", "provider": "BSB"}, /* 1589 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/V95FUkXRY7/manifest.json", "provider": "BSB"}, /* 1592 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/UU1JIZbfI3/manifest.json", "provider": "BSB"}, /* 1594 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/mmArpFxbuZ/manifest.json", "provider": "BSB"}, /* 1595 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/eSAAzM5vhG/manifest.json", "provider": "BSB"}, /* 1596 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/gp2UjhULRr/manifest.json", "provider": "BSB"}, /* 1599 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/DdgHBAXZfh/manifest.json", "provider": "BSB"}, /* 1600 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/28ox4a3u2T/manifest.json", "provider": "BSB"}, /* 1605 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/JX2mGQwiyq/manifest.json", "provider": "BSB"}, /* 1606 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/vF7miJ5IZ7/manifest.json", "provider": "BSB"}, /* 1675 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/opc8fWqq4t/manifest.json", "provider": "BSB"}, /* 1682 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/AmttjqD2xI/manifest.json", "provider": "BSB"}, /* 1710 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/lYuIcVjmOR/manifest.json", "provider": "BSB"}, /* 1717 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/q5cXLqMzKW/manifest.json", "provider": "BSB"}, /* 1728 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/ouKeBwmELV/manifest.json", "provider": "BSB"}, /* 1730 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/Qh6CIVXVom/manifest.json", "provider": "BSB"}, /* 1732 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/qpc8dYsh4q/manifest.json", "provider": "BSB"}, /* 1747 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/yV2WYdyVY7/manifest.json", "provider": "BSB"}, /* 1760 */
          {"manifestId": "https://raw.githubusercontent.com/OscarSeip/oscarseip.github.io/master/Manifests/AesXIRvj4Y/manifest.json", "provider": "BSB"}, /* 1768 */

          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10030121", "provider": "BSB"},
          
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10072043", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11224613", "provider": "BSB"},
          
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10030120", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10586557", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10055450", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10899423", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10769903", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10769900", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10609773", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10368749", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10769906", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10596526", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10352834", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10863328", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10936916", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10596525", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10804653", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10470712", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10769901", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10691594", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10226895", "provider": "BSB"},
         
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11282679", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11267997", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10255363", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11096951", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11137556", "provider": "BSB"},
          
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11281739", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11293291", "provider": "BSB"},
          
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11105827", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10328239", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10739337", "provider": "BSB"},
         
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10287810", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10279836", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10322121", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10952606", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10679289", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10057717", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10515656", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10560961", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10042622", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11347871", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11196483", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11057784", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11202980", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11197119", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11224760", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11224759", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11224765", "provider": "BSB"},
        
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11197118", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11197120", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10007380", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10016535", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10005769", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10040816", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb11216105", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00094155", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10122893", "provider": "BSB"},
          {"manifestId": "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10574916", "provider": "BSB"},
      ]
};

Mirador.viewer(config, [
  ...annotationPlugins,
]);
