#!/usr/bin/env python3

import os
import sys
import argparse
import logging
from urllib.request import urlopen
from iiif_prezi.loader import ManifestReader

def load_manifest(url):
    with urlopen(url) as fh:
        data = fh.read()
        try:
            logging.info(f"loading URL {url}")
            reader = ManifestReader(data)
            mfst = reader.read()
            #js = mfst.toJSON()
            return mfst
        except Exception as e:
            print("   => %s: %s" % (e.__class__.__name__, e))


def manifest_info(manif):
    logging.info(f"manifest id={manif.id}")
    logging.info(f"  label={manif.label}")
    logging.info(f"  description={manif.description}")
    logging.info(f"  attribution={manif.attribution}")
    logging.info(f"  license={manif.license}")
    logging.info(f"  metadata({len(manif.metadata)})")
    for meta in manif.metadata:
        logging.info(f"    label={meta['label']} value={meta['value']}")
    logging.info(f"  sequences({len(manif.sequences)})")

    for seq in manif.sequences:
        logging.info(f"    label={seq.label}, {len(seq.canvases)} canvases")

    logging.info(f"  structures({len(manif.structures)})")
    for struct in manif.structures:
        logging.info(f"    label={struct.label}, {len(struct.canvases)} canvases, {len(struct.ranges)} ranges")


##
## main
##
argp = argparse.ArgumentParser(description='Download and modify IIIF manifests.')
argp.add_argument('--version', action='version', version='%(prog)s 0.1')
argp.add_argument('uri', nargs='+',
                    help='manifest URIs')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

# set up 
logging.basicConfig(level=args.loglevel)

urls = args.uri

if __name__ == "__main__":
    mf = load_manifest(urls[0])
    manifest_info(mf)